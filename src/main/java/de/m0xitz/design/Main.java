package de.m0xitz.design;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import de.m0xitz.design.listeners.PlayerListener;
import de.m0xitz.design.misc.ItemBuilder;
import de.m0xitz.design.misc.ScoreboardManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

public class Main extends JavaPlugin {

    private ProtocolManager protocolManager;
    private PlayerListener playerListener;
    private ScoreboardManager scoreboardManager;

    @Override
    public void onEnable( ) {
        protocolManager = ProtocolLibrary.getProtocolManager( );
        playerListener = new PlayerListener( this );
        scoreboardManager = new ScoreboardManager( this );
    }

    public ProtocolManager getProtocolManager( ) {
        return protocolManager;
    }

    public PlayerListener getPlayerListener( ) {
        return playerListener;
    }

    public ScoreboardManager getScoreboardManager( ) {
        return scoreboardManager;
    }


    public ItemBuilder item( Material material ) {
        return new ItemBuilder( material );
    }

    public ItemBuilder item( Material material, int amount ) {
        return new ItemBuilder( material, amount );
    }

    public ItemBuilder item( Material material, int amount, short data ) {
        return new ItemBuilder( material, amount, data );
    }

    public ItemBuilder item( Player player ) {
        return new ItemBuilder( player );
    }

    public ItemBuilder item( String uuid, String name ) {
        return new ItemBuilder( uuid, name );
    }

    public ItemBuilder item( PotionType potionType, PotionEffect potionEffect, boolean splash ) {
        return new ItemBuilder( potionType, potionEffect, splash );
    }

    public ItemBuilder item( String url ) {
        return new ItemBuilder( url );
    }
}
