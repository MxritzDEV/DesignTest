package de.m0xitz.design.listeners;

import com.google.common.collect.Maps;
import de.m0xitz.design.Main;
import de.m0xitz.design.misc.Rank;
import de.m0xitz.design.misc.ScoreboardBuilder;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.event.server.ServerListPingEvent;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.UUID;

public class PlayerListener implements Listener {

    private Main plugin;
    private HashMap< UUID, String > rankCache;
    private HashMap< String, Rank > ranks;

    public PlayerListener( Main plugin ) {
        this.plugin = plugin;
        this.rankCache = Maps.newHashMap( );
        this.ranks = Maps.newHashMap( );

        ranks.put( "SPIELER", new Rank( ) {
            @Override
            public String getName( ) {
                return "Spieler";
            }

            @Override
            public String getShortName( ) {
                return "Spieler";
            }

            @Override
            public String getColor( ) {
                return "§a";
            }

            @Override
            public String getTeamName( ) {
                return "10SPIELER";
            }

            @Override
            public boolean useFullName( ) {
                return false;
            }
        } );
        ranks.put( "PREMIUM", new Rank( ) {
            @Override
            public String getName( ) {
                return "Premium";
            }

            @Override
            public String getShortName( ) {
                return "Premium";
            }

            @Override
            public String getColor( ) {
                return "§6";
            }

            @Override
            public String getTeamName( ) {
                return "09PREMIUM";
            }

            @Override
            public boolean useFullName( ) {
                return false;
            }
        } );
        ranks.put( "PREMIUM+", new Rank( ) {
            @Override
            public String getName( ) {
                return "Premium+";
            }

            @Override
            public String getShortName( ) {
                return "Premium";
            }

            @Override
            public String getColor( ) {
                return "§6";
            }

            @Override
            public String getTeamName( ) {
                return "09PREMIUM";
            }

            @Override
            public boolean useFullName( ) {
                return false;
            }
        } );
        ranks.put( "VIP", new Rank( ) {
            @Override
            public String getName( ) {
                return "VIP";
            }

            @Override
            public String getShortName( ) {
                return "VIP";
            }

            @Override
            public String getColor( ) {
                return "§5";
            }

            @Override
            public String getTeamName( ) {
                return "08VIP";
            }

            @Override
            public boolean useFullName( ) {
                return true;
            }
        } );
        ranks.put( "BUILDER", new Rank( ) {
            @Override
            public String getName( ) {
                return "Builder";
            }

            @Override
            public String getShortName( ) {
                return "Builder";
            }

            @Override
            public String getColor( ) {
                return "§3";
            }

            @Override
            public String getTeamName( ) {
                return "07BUILDER";
            }

            @Override
            public boolean useFullName( ) {
                return true;
            }
        } );
        ranks.put( "SUPPORTER", new Rank( ) {
            @Override
            public String getName( ) {
                return "Supporter";
            }

            @Override
            public String getShortName( ) {
                return "Sup";
            }

            @Override
            public String getColor( ) {
                return "§9";
            }

            @Override
            public String getTeamName( ) {
                return "06SUPPORTER";
            }

            @Override
            public boolean useFullName( ) {
                return true;
            }
        } );
        ranks.put( "MODERATOR", new Rank( ) {
            @Override
            public String getName( ) {
                return "Moderator";
            }

            @Override
            public String getShortName( ) {
                return "Mod";
            }

            @Override
            public String getColor( ) {
                return "§c";
            }

            @Override
            public String getTeamName( ) {
                return "05MODERATOR";
            }

            @Override
            public boolean useFullName( ) {
                return true;
            }
        } );
        ranks.put( "SRMODERATOR", new Rank( ) {
            @Override
            public String getName( ) {
                return "SrModerator";
            }

            @Override
            public String getShortName( ) {
                return "SrMod";
            }

            @Override
            public String getColor( ) {
                return "§c";
            }

            @Override
            public String getTeamName( ) {
                return "04SRMODERATOR";
            }

            @Override
            public boolean useFullName( ) {
                return true;
            }
        } );
        ranks.put( "CONTENT", new Rank( ) {
            @Override
            public String getName( ) {
                return "Content";
            }

            @Override
            public String getShortName( ) {
                return "Content";
            }

            @Override
            public String getColor( ) {
                return "§b";
            }

            @Override
            public String getTeamName( ) {
                return "03CONTENT";
            }

            @Override
            public boolean useFullName( ) {
                return true;
            }
        } );
        ranks.put( "DEVELOPER", new Rank( ) {
            @Override
            public String getName( ) {
                return "Developer";
            }

            @Override
            public String getShortName( ) {
                return "Dev";
            }

            @Override
            public String getColor( ) {
                return "§b";
            }

            @Override
            public String getTeamName( ) {
                return "02DEVELOPER";
            }

            @Override
            public boolean useFullName( ) {
                return true;
            }
        } );
        ranks.put( "ADMIN", new Rank( ) {
            @Override
            public String getName( ) {
                return "Administrator";
            }

            @Override
            public String getShortName( ) {
                return "Admin";
            }

            @Override
            public String getColor( ) {
                return "§4";
            }

            @Override
            public String getTeamName( ) {
                return "01ADMIN";
            }

            @Override
            public boolean useFullName( ) {
                return true;
            }
        } );

        plugin.getServer( ).getPluginManager( ).registerEvents( this, plugin );
    }

    @EventHandler
    public void onJoin( PlayerJoinEvent event ) {
        Player player = event.getPlayer( );

        next( player );
        setDisplay( player );

        event.setJoinMessage( null );
    }

    @EventHandler
    public void onChat( AsyncPlayerChatEvent event ) {
        Player player = event.getPlayer( );
        Rank rank = getRank( player );


        event.setFormat( rank.getColor( ) + "%s §8» §f%s" );
    }

    @EventHandler
    public void onSneak( PlayerToggleSneakEvent event ) {
        Player player = event.getPlayer( );
        if ( event.isSneaking( ) )
            return;
        next( player );
        setDisplay( player );
    }

    @EventHandler
    public void onFlight( PlayerToggleFlightEvent event ) {
        Player player = event.getPlayer( );
        if ( player.isSneaking( ) ) {
            player.kickPlayer( "§cDu wurdest §4TEMPORÄR §cvon §bVentage§8.§beu §cgebannt§8.\n" +
                    "§7Grund §8➟ §cClientmods\n" +
                    "§7Dauer §8➟ §c29 Tage 23 Stunden 1 Minute\n\n" +
                    "§7§m-------------------------------§r\n\n" +
                    "§aDu kannst einen Entbannungsantrag auf dem §eTeamspeak §8[§7ts.ventage.eu§8] §astellen§8." );
            return;
        }
        player.kickPlayer( "§cDu wurdest von §bVentage§8.§beu §cgekickt§8.\n" +
                "§7Grund §8➟ §cBugusing" );
    }

    @EventHandler
    public void onPing( ServerListPingEvent event ) {
        event.setMotd( "§bVentage§8.§beu §8× §7your §eminigame §7server §8× §6§oMinecraft §61.8\n" +
                "§cmaintenance §8➟ §7Follow us on §bTwitter§8: §f@VentageEU " );
    }

    public void set( Player player, String rank ) {
        rankCache.put( player.getUniqueId( ), rank );
    }

    public boolean hasRank( Player player ) {
        return rankCache.containsKey( player.getUniqueId( ) );
    }

    public String get( Player player ) {
        return rankCache.get( player.getUniqueId( ) );
    }

    public String next( Player player ) {
        String rank = hasRank( player ) ? rankCache.get( player.getUniqueId( ) ) : "ADMIN";
        switch ( rank ) {
            case "SPIELER":
                rank = "PREMIUM";
                break;
            case "PREMIUM":
                rank = "PREMIUM+";
                break;
            case "PREMIUM+":
                rank = "VIP";
                break;
            case "VIP":
                rank = "BUILDER";
                break;
            case "BUILDER":
                rank = "SUPPORTER";
                break;
            case "SUPPORTER":
                rank = "MODERATOR";
                break;
            case "MODERATOR":
                rank = "SRMODERATOR";
                break;
            case "SRMODERATOR":
                rank = "CONTENT";
                break;
            case "CONTENT":
                rank = "DEVELOPER";
                break;
            case "DEVELOPER":
                rank = "ADMIN";
                break;
            default:
                rank = "SPIELER";
                break;
        }
        set( player, rank );
        return rank;
    }

    public void setDisplay( Player player ) {
        for ( int i = 0; i < 250; i++ ) {
            player.sendMessage( "" );
        }
        Rank rank = ranks.get( get( player ) );

        plugin.getScoreboardManager( ).setPlayer( player );

        ScoreboardBuilder scoreboard = plugin.getScoreboardManager( ).setupScoreboard( player );

        /*scoreboard.setLine( 12, "§1" );
        scoreboard.setLine( 11, rank.getColor( ) + "✎ §8┃ §7Dein Rang§8:" );
        scoreboard.setLine( 10, " §8»  " + rank.getColor( ) + rank.getName( ) );
        scoreboard.setLine( 9, "§2" );
        scoreboard.setLine( 8, "§6✪ §8┃ §7Coins§8:" );
        scoreboard.setLine( 7, " §8»  §e12313" );
        scoreboard.setLine( 6, "§3" );
        scoreboard.setLine( 5, "§2⚔ §8┃ §7Clan§8:" );
        scoreboard.setLine( 4, " §8»  §a#0x" );
        scoreboard.setLine( 3, "§4" );
        scoreboard.setLine( 2, "§9✆ §8┃ §7TeamSpeak§8:" );
        scoreboard.setLine( 1, " §8»  §bts.ventage.eu" );*/

        scoreboard.setLine( 12, "§1" );
        scoreboard.setLine( 11, rank.getColor( ) + "§7Dein Rang§8:" );
        scoreboard.setLine( 10, "§8➟ " + rank.getColor( ) + rank.getName( ) );
        scoreboard.setLine( 9, "§2" );
        scoreboard.setLine( 8, "§7Coins§8:" );
        scoreboard.setLine( 7, "§8➟ §e12313" );
        scoreboard.setLine( 6, "§3" );
        scoreboard.setLine( 5, "§7Clan§8:" );
        scoreboard.setLine( 4, "§8➟ §a#0x" );
        scoreboard.setLine( 3, "§4" );
        scoreboard.setLine( 2, "§7TeamSpeak§8:" );
        scoreboard.setLine( 1, "§8➟ §bts.ventage.eu" );

        scoreboard.setup( );

        player.sendMessage( "§8┃ §aSurvivalGames §8» §7Die Runde startet in §e30 §7Sekunden§8." );
        player.sendMessage( "§8┃ §aSurvivalGames §8» §cDer Server restartet in §e10 §cSekunden§8." );
        player.sendMessage( "§8┃ §aSurvivalGames §8» " + rank.getColor( ) + player.getName( ) + " §7hat das Spiel betreten§8. [§e" + plugin.getServer( ).getOnlinePlayers( ).size( ) + "§8/§716§8]" );
        player.sendMessage( "§8┃ §aSurvivalGames §8» " + rank.getColor( ) + player.getName( ) + " §7wurde von §aGommeHD §8[§a10§4❤§8] §7getötet§8." );
        player.sendMessage( "§8┃ §aSurvivalGames §8» " + rank.getColor( ) + player.getName( ) + " §7ist gestorben§8." );
        player.sendMessage( "§8┃ §aSurvivalGames §8» " + rank.getColor( ) + player.getName( ) + " §7hat das Spiel verlasen§8. [§e" + plugin.getServer( ).getOnlinePlayers( ).size( ) + "§8/§716§8]" );

        String header = "\n" +
                "§8» §bVentage§8.§beu §8× §7your §eminigame §7server §8«\n" +
                "§7Server §8➟ §eLobby-1\n";
        String footer = "\n" +
                "§7TeamSpeak §8➟ §ets.ventage.de  §8┃  §7Twitter §8➟ §e@VentageEU\n" +
                "§7Hoster §8➟ §bmyprepaidhost.de\n";

        IChatBaseComponent headerComponent = IChatBaseComponent.ChatSerializer.a( "{\"text\":\"" + header + "\"}" );
        IChatBaseComponent footerComponent = IChatBaseComponent.ChatSerializer.a( "{\"text\":\"" + footer + "\"}" );

        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter( headerComponent );

        try {
            Field field = packet.getClass( ).getDeclaredField( "b" );
            field.setAccessible( true );
            field.set( packet, footerComponent );
            field.setAccessible( false );
        } catch ( NoSuchFieldException | IllegalAccessException ex ) {
            ex.printStackTrace( );
        }

        ( ( CraftPlayer ) player ).getHandle( ).playerConnection.sendPacket( packet );
    }

    public Rank getRank( Player player ) {
        return ranks.get( get( player ) );
    }
}
