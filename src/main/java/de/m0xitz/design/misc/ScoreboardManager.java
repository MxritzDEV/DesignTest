package de.m0xitz.design.misc;

import de.m0xitz.design.Main;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScoreboardManager {

    private Main plugin;

    // ×
    public ScoreboardManager( Main plugin ) {
        this.plugin = plugin;
    }

    public void setTeam( Player player, Team team ) {
        Rank rank = plugin.getPlayerListener( ).getRank( player );
        String prefix = "";
        if ( rank.useFullName( ) ) {
            prefix = rank.getColor( ) + rank.getShortName( ) + " §8┃ " + rank.getColor( );
        } else {
            prefix = rank.getColor( );
        }

        team.setPrefix( prefix );
        team.addPlayer( player.getPlayer( ) );
    }

    public void setPlayer( Player player ) {
        updatePlayer( player );
        for ( Player all : plugin.getServer( ).getOnlinePlayers( ) ) {
            if ( all.equals( player ) )
                continue;
            Rank rank = plugin.getPlayerListener( ).getRank( all );
            Scoreboard scoreboard = player.getScoreboard( ) == null ? plugin.getServer( ).getScoreboardManager( ).getNewScoreboard( ) : player.getScoreboard( );
            Team team = scoreboard.getTeam( rank.getTeamName( ) ) == null ? scoreboard.registerNewTeam( rank.getTeamName( ) ) : scoreboard.getTeam( rank.getTeamName( ) );
            setTeam( all, team );
            player.setScoreboard( scoreboard );
        }
    }

    public void updatePlayer( Player player ) {
        Rank rank = plugin.getPlayerListener( ).getRank( player );
        for ( Player all : plugin.getServer( ).getOnlinePlayers( ) ) {
            Scoreboard scoreboard = player.getScoreboard( ) == null ? plugin.getServer( ).getScoreboardManager( ).getNewScoreboard( ) : player.getScoreboard( );
            Team team = scoreboard.getTeam( rank.getTeamName( ) ) == null ? scoreboard.registerNewTeam( rank.getTeamName( ) ) : scoreboard.getTeam( rank.getTeamName( ) );
            setTeam( player, team );
            all.setScoreboard( scoreboard );
        }
    }

    public ScoreboardBuilder setupScoreboard( Player player ) {
        ScoreboardBuilder scoreboard = new ScoreboardBuilder( plugin, player.getPlayer( ), "§8» §bVentage§8.§beu" );
        if ( player.hasMetadata( "scoreboard" ) )
            player.removeMetadata( "scoreboard", plugin );
        player.setMetadata( "scoreboard", new FixedMetadataValue( plugin, scoreboard ) );

        return scoreboard;
    }

    public void editLine( Player player, int line, String text ) {
        ScoreboardBuilder scoreboard = ( ScoreboardBuilder ) player.getMetadata( "scoreboard" ).get( 0 );
        scoreboard.setLine( line, text );
    }

    public void removeLine( Player player, int line ) {
        ScoreboardBuilder scoreboard = ( ScoreboardBuilder ) player.getMetadata( "scoreboard" ).get( 0 );
        scoreboard.removeLine( line );
    }

}
