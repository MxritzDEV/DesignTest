package de.m0xitz.design.misc;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ItemBuilder {

    private ItemStack item;

    private Class< ? > skullMetaClass;
    private Class< ? > tileEntityClass;
    private Class< ? > blockPositionClass;

    private final List< Material > leatherArmor = Lists.newArrayList( Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS );

    public ItemBuilder( Material material ) {
        setClasses( );
        this.item = new ItemStack( material );
    }

    public ItemBuilder( Material material, int amount ) {
        setClasses( );
        this.item = new ItemStack( material, amount );
    }

    public ItemBuilder( Material material, int amount, short data ) {
        setClasses( );
        this.item = new ItemStack( material, amount, data );
    }

    public ItemBuilder( String url ) {
        setClasses( );
        ItemStack head = new ItemStack( Material.SKULL_ITEM, 1, ( short ) 3 );
        if ( url.isEmpty( ) ) {
            this.item = head;
            return;
        }
        if ( !url.contains( "textures.minecraft.net" ) ) {
            this.item = head;
            return;
        }
        SkullMeta headMeta = ( SkullMeta ) head.getItemMeta( );
        GameProfile gameProfile = new GameProfile( UUID.randomUUID( ), null );
        byte[] encodedData = Base64.encodeBase64( String.format( "{textures:{SKIN:{url:\"%s\"}}}", url ).getBytes( ) );
        gameProfile.getProperties( ).put( "textures", new Property( "textures", new String( encodedData ) ) );
        Field profileField;
        try {
            profileField = headMeta.getClass( ).getDeclaredField( "profile" );
            profileField.setAccessible( true );
            profileField.set( headMeta, gameProfile );
        } catch ( NoSuchFieldException | IllegalAccessException e ) {
            e.printStackTrace( );
        }
        head.setItemMeta( headMeta );
        this.item = head;
    }

    public ItemBuilder( PotionType type, PotionEffect effect, boolean splash ) {
        setClasses( );
        Potion potion = new Potion( type );
        potion.getEffects( ).clear( );
        potion.setSplash( splash );
        this.item = potion.toItemStack( 1 );
        PotionMeta meta = ( PotionMeta ) this.item.getItemMeta( );
        meta.getCustomEffects( ).clear( );
        meta.addCustomEffect( effect, true );
        this.item.setItemMeta( meta );
    }

    public ItemBuilder( Player player ) {
        setClasses( );
        this.item = new ItemStack( Material.SKULL_ITEM, 1, ( short ) 3 );
        SkullMeta meta = ( SkullMeta ) this.item.getItemMeta( );
        meta.setDisplayName( player.getCustomName( ) );
        meta.setOwner( player.getName( ) );
        this.item.setItemMeta( meta );
    }

    public ItemBuilder( String uuid, String name ) {
        setClasses( );
        this.item = new ItemStack( Material.SKULL_ITEM, 1, ( short ) 3 );
        SkullMeta meta = ( SkullMeta ) this.item.getItemMeta( );
        meta.setDisplayName( name );
        meta.setOwner( name );
        this.item.setItemMeta( meta );
    }

    private void setClasses( ) {
        String version = Bukkit.getServer( ).getClass( ).getPackage( ).getName( ).split( "\\." )[3];
        try {
            skullMetaClass = Class.forName( "org.bukkit.craftbukkit." + version + ".inventory.CraftMetaSkull" );
            tileEntityClass = Class.forName( "net.minecraft.server." + version + ".TileEntitySkull" );
            blockPositionClass = Class.forName( "net.minecraft.server." + version + ".BlockPosition" );
        } catch ( ClassNotFoundException e ) {
        }
    }


    public ItemBuilder setAmount( int amount ) {
        this.item.setAmount( amount );
        return this;
    }


    public ItemBuilder setData( short data ) {
        this.item.setDurability( data );
        return this;
    }


    public ItemBuilder setUnbreakable( boolean unbreakable ) {
        this.item.getItemMeta( ).spigot( ).setUnbreakable( unbreakable );
        return this;
    }


    public ItemBuilder setMeta( ItemMeta itemMeta ) {
        this.item.setItemMeta( itemMeta );
        return this;
    }


    public ItemBuilder setDisplayName( String displayName ) {
        this.item.getItemMeta( ).setDisplayName( displayName );
        return this;
    }


    public ItemBuilder setDurablility( short durablility ) {
        this.item.setDurability( durablility );
        return this;
    }


    public ItemBuilder setLore( List< String > lore ) {
        this.item.getItemMeta( ).setLore( lore );
        return this;
    }


    public ItemBuilder setLore( String... lore ) {
        this.item.getItemMeta( ).setLore( Arrays.asList( lore ) );
        return this;
    }


    public ItemBuilder addEnchantment( Enchantment enchantment, int level ) {
        this.item.getItemMeta( ).addEnchant( enchantment, level, true );
        return this;
    }


    public ItemBuilder setSkullOwner( String owner ) {
        if ( this.item.getType( ) != Material.SKULL_ITEM )
            return this;
        ( ( SkullMeta ) this.item.getItemMeta( ) ).setOwner( owner );
        return this;
    }


    public ItemBuilder setColor( int red, int green, int blue ) {
        if ( !this.leatherArmor.contains( this.item.getType( ) ) )
            return this;
        ( ( LeatherArmorMeta ) this.item.getItemMeta( ) ).setColor( Color.fromRGB( red, green, blue ) );
        return this;
    }


    public ItemBuilder setColor( Color color ) {
        if ( !this.leatherArmor.contains( this.item.getType( ) ) )
            return this;
        ( ( LeatherArmorMeta ) this.item.getItemMeta( ) ).setColor( color );
        return this;
    }


    public ItemBuilder addGlow( ) {
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy( this.item );
        NBTTagCompound tag = null;
        if ( !nmsStack.hasTag( ) ) {
            tag = new NBTTagCompound( );
            nmsStack.setTag( tag );
        }
        if ( tag == null )
            tag = nmsStack.getTag( );
        NBTTagList ench = new NBTTagList( );
        tag.set( "ench", ench );
        nmsStack.setTag( tag );
        this.item = CraftItemStack.asCraftMirror( nmsStack );
        return this;
    }


    public ItemBuilder setAuthor( String author ) {
        if ( this.item.getType( ) != Material.WRITTEN_BOOK )
            return this;
        ( ( BookMeta ) this.item.getItemMeta( ) ).setAuthor( author );
        return this;
    }


    public ItemBuilder setTitle( String title ) {
        if ( this.item.getType( ) != Material.WRITTEN_BOOK )
            return this;
        ( ( BookMeta ) this.item.getItemMeta( ) ).setTitle( title );
        return this;
    }


    public ItemBuilder addPage( String... pages ) {
        if ( this.item.getType( ) != Material.WRITTEN_BOOK )
            return this;
        ( ( BookMeta ) this.item.getItemMeta( ) ).setPages( pages );
        return this;
    }


    public String[] getLore( ) {
        return this.item.getItemMeta( ).getLore( ).toArray( new String[0] );
    }


    public ItemBuilder hideAttributes( ) {
        ItemMeta meta = this.item.getItemMeta( );
        meta.addItemFlags( ItemFlag.values( ) );
        this.item.setItemMeta( meta );
        return this;
    }


    public ItemStack build( ) {
        return this.item;
    }


    public String getDisplayName( ) {
        return this.item.getItemMeta( ).getDisplayName( );
    }


    public ItemBuilder clone( ) {
        return this.clone( );
    }

}
