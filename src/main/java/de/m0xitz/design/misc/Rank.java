package de.m0xitz.design.misc;

public interface Rank {


    String getName( );

    String getShortName( );

    String getColor( );

    String getTeamName( );

    boolean useFullName( );
}
